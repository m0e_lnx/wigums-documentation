
#############################
|vector_edition| SOHO Edition
#############################

What´s in the SOHO Edition?
=========================== 

|vector_edition| SOHO, as its name implies, is a Linux distribution aimed at
Small Office and Home Office users - people who want the latest fully
featured desktop environment and applications but also enjoy speed! As
of March 1st, 2009 the SOHO version 6.0 is still in development and
release of a final version is expected by before mid 2009. In the
meantime our SOHO 5.9.1 is still available for download. VL SOHO comes
provides you with:

-  a choice of either a graphical or text-based installer with great
   hardware detection. Install time is only about 15-20 minutes on
   modern computers.
-  KDE, a great looking and comprehensive desktop environment, complete
   with a huge range of applications.
-  Fluxbox, a lean and mean window manager, customized with the "wbar"
   launcher - for those wanting a less resource intensive environment
   than KDE.
-  The VASMCC user-friendly yet powerful system administration utility.
-  gslapt and slapt-get for easy, automated package downloading,
   upgrading, installation and maintenance.
-  either HAL or *vl-hot*, for automatic mounting of external USB,
   Firewire and PCMCIA external storage devices.
-  OpenOffice 3 productivity suite.
-  Business applications for personal information, organization,
   finance, and databases.
-  Powerful web development systems.
-  Simple to advanced graphics editors.
-  Complete Internet applications. In particular we have preconfigured
   Firefox and Opera with PDF, MPlayer, FlashPlayer and Java plugins.
-  Multimedia and entertainment - for music and video and web content
   streaming.
-  Internet, Network Neighbourhood and wireless connectivity.
-  Support for peripherals including printers, scanners, digital cameras
   and fax/modems.

All of that comes with Vector Linux´s trademark characteristics of being
fast, light, well integrated and stable. You can `view
screenshots <http://www.vectorlinux.com/mod.php?mod=userpage&menu=10&page_id=10>`_
of VL SOHO at our website.*We hope you enjoy it!*


Requirements
============

Vector Linux SOHO is the relative "heavy-weight" compared to the
Standard version of VL, and given that it runs the KDE Desktop
Environment, SOHO requires more space and memory. While better suited
for today's modern computers, SOHO is significantly faster and
'bloat-free' than many other Linux distributions running KDE or Gnome.
That said, the following system requirements are recommended for our
SOHO version:

-  Processor: Pentium III compatible or better (so you may use an AMD
   processor).
-  Harddisk: 3.6 GB for the system, plus space for swap (virtual memory)
   and personal files.
-  Memory: 256 MB absolute minimum, 512MB or more for speed.
-  Video card and Monitor capable of 1024x768 resolution and 24 bit
   color.
-  Standard mouse, keyboard, sound card, CDROM, etc.

If you are ready to try our VL SOHO Edition it is available for purchase
from our `CD Store <http://vectorlinux.com/cd-store>`_. On the other
hand it is possible to add the KDE and OpenOffice packages to VL 6.0
Standard to approach closely the capabilities of VL SOHO.

