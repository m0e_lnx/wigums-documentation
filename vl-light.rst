
##############################
|vector_edition| Light Edition
##############################


What´s in the Light Edition?
============================

Vector Linux Light provides a lean and mean Linux system. It is
optimized for use on older computers and is surprisingly lightweight,
fast and stable. Despite being "light weight", it is a complete Linux
workstation system, including compilers, multimedia and office
applications. The most recent release of VL Light is still in beta
development stage, up to version Beta 2 as at February 25th, 2009. It is
not recommended that newbies install this system until it has been
finalized.

|vector_edition| Light is unlike smaller Linux distros such as Puppy, DSL,
Feather and others. Instead it includes a full development environment
for programming and compiling, scripting languages, man pages,
documentation, a complete console based on bash (rather than busybox)
and a huge array of typical Linux command line utilities and software.
It also has these features:

-  a fun, fast and easy installer with good hardware detection. Install
   time is only about 10-12 minutes on modern computers.
-  the latest IceWM and JWM window managers with the PCManFM desktop
   manager - both are good-looking and fast window managers which
   consume few system resources.
-  VASM: a powerful system administration utility.
-  VASMCC: a user-friendly GUI interface to VASM.
-  gslapt and slapt-get for easy, automated package downloading,
   upgrading, installation and maintenance.
-  HAL or vl-hot systems, for detection and auto-mounting of USB storage
   devices, low resources.
-  Opera browser.
-  Many other Internet applications including Pidgin, XChat, gFTP, Lynx,
   nmapFE, pyNeighborhood.
-  vcpufreq: for reducing CPU frequency on supported processors, mainly
   for laptops to save power.
-  vwifi: a special version of WiFi manager for help configuring various
   wireless cards and networks.
-  MPlayer, Xine and xmms for multimedia and entertainment - for music
   and video and web content streaming.
-  vsmbmount: a Samba connectivity tool.
-  vpackager: an automated system for compiling ad packaging VL packages
   from source code.
-  CUPS Printing System


Requirements
============

It is recommended that your system meets these requirements:

-  Pentium 200 or better compatible processor (i586 and up).
-  minimum of 2GB of hard disk space for the entire system (less if you
   choose to install only certain packages), plus additional space for
   swap (virtual memory) and personal files.
-  64 MB of memory as an absolute minimum - but 256 MB or more will
   greatly improve performance.
-  Suitable Video card and Monitor.
-  Standard mouse, keyboard, sound card, CDROM, etc.

If you are ready to try VL Light it is free to download.

