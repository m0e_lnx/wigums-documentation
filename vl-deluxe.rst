
###############################
|vector_edition| Deluxe Edition
###############################

What is the Deluxe Edition?
===========================

.. image:: _static/images/vldeluxe.gif
   :align: center
   :alt: Deluxe

Vector Linux DELUXE now comes in two Editions: Deluxe SOHO and Deluxe
Standard. Both are available from our CD-Store. As at March 1st, 2009
Deluxe Standard is at version 6.0 while Deluxe SOHO is still at Version
5.9, although we expect a version 6.0 of Deluxe SOHO later in 2009. The
Deluxe Editions are intended for professionals, extending the SOHO and
Standard editions with up to 1000 MB of additional software. The extra
applications can be installed individually to build the system exactly
as you need it.

The two CD package set of Deluxe Standard features a custom XFCE desktop
with popular applications like Amarok, Blender, and the Gimp. Additional
included applications are KDE 4.2, OpenOffice 3 and E17 amongst many
others, particularly multimedia applications. Fourteen days of
professional installation and configuration support included. There is
automatic support for printers, scanners, USB hardware and CDRW / DVD
drives. There are several new multimedia programs and libraries, the
latest network applications and development programs along with their
needed libraries. We have also included custom graphics only available
in the DELUXE version. Click
`HERE <http://vectorlinux.com/screenshots>`_ for screenshots.

The second bonus CDROM is built around our own community produced VL
MULTIMEDIA BONUS DISK. This is a gigantic set of multimedia orientated
applications and libraries that allow you to perform anything from dvd
authoring to full audio/video editing. A complete list of included
programs and screenshots can be found on the `VL
website <http://www.vectorlinux.com>`_.


Support |vector_edition|
========================
`CD sales`_ and |vector_edition| Goodies_ are the only form of revenue 
available to |vector_edition| to help offset the costs of running this 
small business. We have put a great deal of time, energy and love into 
the formulation of this product, and your generosity and support is 
sincerely appreciated. In addition to the financial support, your purchase 
of the Deluxe Edition lets us know that you care and that you would like us 
to continue developing and producing a better Linux distribution for you!
The purchase of either of the |vector_edition| Deluxe Editions is also
probably a good idea for those with a slow Internet connection, where
downloading an entire ISO file is not a viable option.


Requirements
============

The requirements for the Deluxe Standard Edition can vary according to
what you decide to install, however we would recommend a system that
meets the following requirements so that you can enjoy even the most
demanding applications:

-  Processor: Pentium III compatible or better (so you may use an AMD
   processor).
-  Hard Disk: 6.5GB for the entire 2 CD system, plus some more for your
   data.
-  Memory: 256 MB for comfort, 512MB or more, to truly enjoy all the
   multimedia applications.
-  Video card and Monitor capable of at least 1024x768 resolution, 24
   bit color.
-  Standard mouse, keyboard, sound card, CDROM, etc.

The requirements for the Deluxe SOHO Edition can vary according to what
you decide to install, however we would recommend a system that meets
the following requirements so that you can enjoy even the most demanding
applications:

-  Processor: Pentium IV compatible or better (so you may use an AMD
   processor).
-  Hard Disk: 6.5GB for the entire system, plus some more for your data.
-  Memory: 512MB or more.
-  Video card and Monitor capable of 1024x768 resolution, 24 bit color.
-  Standard mouse, keyboard, sound card, CDROM, etc.


.. _CD sales: http://vectorlinux.com/cd-store
.. _Goodies: http://www.cafepress.com/vectorlinux/
