
#############################
|vector_edition| Live Edition
#############################

The |vector_edition| Live Edition is a bootable CDROM which contain the
entire |vector_edition| operating system on the CD. This enables the user to
test a distribution on their system without the need of re-partitioning
and installing to the hard drive. This Live version also enables the
user to use their favorite operating system on any computer, without
affecting the computer for future users. If you wish you may install the
Live Edition to your hard drive, but it is recommended you choose our
Standard, SOHO or Light Edition instead if you wish to do this.

Using the VL Live CD requires the following:

-  BIOS configured so that the CDROM/DVD drive is set to boot first in
   the list of boot devices
-  an absolute minimum of 256MB of RAM, in fact you probably need 512MB
   for a good experience
-  the password for all users, including "root" is "vector"
-  patience: the entire operating system and all applications are loaded
   into RAM as required from your relatively slow optical drive. Optical
   drive read speeds are *much* slower than hard drive read speeds. Thus
   Live CD's are really only used as testbeds to see whether you like
   the distro or as repair tools.

As at March 1st, 2009 the most recent version of VL Live is VL5.9
Standard Live. VL Live 5.9 defaults to the xfce desktop environment but
you also have a choice of fluxbox and jwm window managers. A newer
version 6.0 of Standard Live is expected possibly mid 2009. Stay tuned.
You can check our website or our forum (links above) for release details
and there you can also learn of the system requirements and included
packages for the upcoming new releases.

