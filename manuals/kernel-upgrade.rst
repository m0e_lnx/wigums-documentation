Kernel Upgrade
==============

Vector Linux offers several kernels. Here i will explain how to add 
a kernel to your system. I say **ADD** because you should **NOT** 
get rid of your working kernel. If for some reason something
goes wrong you will have the original kernel to fall back on.

First of all you need to download a kernel package:

    32 bit kernels: http://vlcore.vectorlinux.com/pkg/stable/veclinux-7.1/extra/kernels/

    64 bit kernels: http://vlcore.vectorlinux.com/pkg/stable/VL64-7.1/extra/kernels/


Then follow these instructions:

- Download the kernel package and the **matching** kernel-modules package
- ``cd`` to the directory you saved the kernel to
- ``installpkg`` both the kernel and kernel-modules packages

Now you need to edit your bootloader to include the new kernel.

- If you use lilo open ``/etc/lilo.conf`` with your editor of choice
- Simply copy the first kernel entry section which looks like this::

    # -- Vector on /dev/sdX --
    image = /boot/vmlinuz
    label = Vector
    initrd = /boot/initrd.gz
    root = "UUID=3b3ed16e-8db3-44be-b4b8-9b81df7692b2"
    append = "quiet splash"
    read-only

    # --

- Paste it at the bottom of ``lilo.conf`` and edit as appropriate
- For example if you installed kernel 4.0.7::

    # -- Vector on /dev/sdX -- 
    image = /boot/vmlinuz-4.0.7
    label = Vector-4.0.7
    initrd = /boot/initrd.gz
    root = "UUID=3b3ed16e-8db3-44be-b4b8-9b81df7692b2"
    append = "quiet splash"
    read-only
    # --

Now run ``lilo``

- ``lilo -v``

That's it. Reboot and you should now have an option to boot 4.0.7 as well as the original kernel


.. warning::

    This doc needs a grub how-to here


