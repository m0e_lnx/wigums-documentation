Packaging Environment (sandboxing)
==================================

This document will describe how to setup and use a clean packaging
environment to build packages locally on your installed |vector_edition|


Assumptions
-----------

This guide will assume the following.

- You have |vector_edition| 7.1 installed.
   This setup cannot be produced in earlier versions of |vector_edition|
- You have read and followed the docker_ guide for |vector_edition| and
   have successfully launched the docker daemon
- You are confortable using the command line or a teminal emulator.
- You are somewhat familiar with how to use git

.. _docker: http://vlcoredocumentation.rtfd.org/en/latest/manuals/docker.html


Task Description
----------------

The clean build environment is achieved by setting up docker and importing
the correct image into your docker installation.

The default docker behaviour is to discard all of the data after each
container has run its life cycle.  In order to save the the packages
you create inside the clean environment, we will need to setup a directory
structure somewhere in your path so the data can be saved there for
access after the container is terminated.


.. note::
   If you are on a 32-bit installation of |vector_edition| you will only
   be able to use 32-bit images.  Users with a 64-bit installation of
   |vector_edition| will be able to use 32-bit and 64-bit images.




Preparations
------------

As pointed out above, you will need to create a directory structure to
save your packages once they have been created.  For this, we will
assume you are in your $HOME directory, but really it does not matter
as long as you know where you placed your data.  Wherever that may be,
create the structure following these commands.

.. note::
   The following snippet assumes you imported the
   ``VL-7.1-BB-FINAL_vlbb-docker.tar.gz`` image and used the tag
   ``vector:7.1-bb`` for it.  See ``docker help import`` or the docker_
   guide for |vector_edition|.
   

::

   mkdir -p sandbox/data-{in,out}
   pushd sandbox/data-in
   git clone http://bitbucket.org/VLCore/vl71
   popd

This has just prepared a directory structure in your host that will be
shared with your docker container.  The ``git clone...`` line would have
downloaded the build scripts collection for |vector_edition| 7.1 and should
be a good starting point for building many applications from source.


Entering the build environment (sandbox)
----------------------------------------

To enter the build environment, you will need to call the ``docker run``
command.  To get familiarized with what the arguments used  mean, see
``docker help run``.

::

   export DATA_IN=$PWD/sandbox/data-in/vl71
   export DATA_OUT=$PWD/sandbox/data-out

   docker run --rm -ti -v $DATA_IN:/home/slackbuilds -v $DATA_OUT:/data \
   vector:7.1-bb /bin/bash

At this point, you should see some output on the terminal and will notice
the prompt has changed.  you should be able to ``cd /data/in/`` and see the
directory named ``vl71`` that you cloned with git before.  You can find
a lot of build scripts here ready to be executed.


Working inside the build environment
------------------------------------

Now that you are inside the clean build environment, you are free to install
whatever you need inside.  There is a variety of tools available to you inside
the sandbox.  The following is a short list of some of the ones you will
most likely need to use inside the sandbox.

- slapt-get
- sbbuilder
- version control applications (svn, git, etc)
- text editors (nano, vi, emacs, etc)

If you happen to need anything else, just use ``slapt-get`` to install it.
