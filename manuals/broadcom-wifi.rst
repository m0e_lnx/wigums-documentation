====================================
Vector Linux 7.2 Broadcom wifi howto
====================================

blacklist modules
===================
1. nano /etc/modprobe.d/blacklist.conf

2. Add these lines to your ``blacklist.conf`` file
 
 :: 

    blacklist b43
    blacklist ssb
    blacklist bcma
    blacklist bcm43xx
    blacklist bcm80211
    blacklist brcmsmac

3. Reboot. You should now have module wl loaded.  
   Verify with ``lsmod | grep wl``

4. Done
